# RPG Heroes

This project is a program that let's a user create Heroes.

# Table of contents

  - [Background](#background)
  - [Requirements](#requirements)
  - [Install](#install)
  - [Usage](#usage)
  - [UML](#UML)
  - [Author](#author)
  - [License](#license)

## Background

This project is made as an assignment for the Noroff Accelerate program.

## Requirements

- Various hero classes having attributes which increase at different rates as the character gains levels.  
- Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of
the hero, causing it to deal more damage and be able to survive longer. Certain heroes can equip certain item
types.   
- Custom exceptions. There are two custom exceptions you are required to write.  
- Testing of the main functionality.  
- CI pipeline to show that all tests are passed.  


## Install

Clone the gitlab repository `git clone https://gitlab.com/JoakimAarskog/rpg-heroes.git`

## Usage

Open the project in [Visual Studio](https://visualstudio.microsoft.com/) and run the program

## UML

![UML Diagram](HeroUML.png)

## Author
Gitlab [@JoakimAarskog](https://gitlab.com/JoakimAarskog)  

## License

MIT




