using FluentAssertions;
using RPGHeroes.Equipment;
using RPGHeroes.Equipment.Enum;
using RPGHeroes.Hero;
using RPGHeroes.Utils.Exeptions;

namespace RPGHeroesTest
{
    public class HeroTests
    {
        #region Weapon
        [Fact]
        [Trait("Weapon", "Initialization")]
        public void Constructor_Initialize_Weapon_Should_Have_Correct_Name()
        {

            var name = "Blade of Steel";
            var reqLevel = 1;
            var damage = 8;
            var weapon = new Weapon(name, reqLevel, WeaponType.Swords, damage);

            var expected = name;
            var actual = weapon.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Weapon", "Initialization")]
        public void Constructor_Initialize_Weapon_Should_Have_Correct_RequiredLevel()
        {

            var name = "Blade of Steel";
            var reqLevel = 1;
            var damage = 8;
            var weapon = new Weapon(name, reqLevel, WeaponType.Swords, damage);

            var expected = reqLevel;
            var actual = weapon.RequiredLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Weapon", "Initialization")]
        public void Constructor_Initialize_Weapon_Should_Have_Correct_WeaponType()
        {

            var name = "Blade of Steel";
            var reqLevel = 1;
            var damage = 8;
            var weapon = new Weapon(name, reqLevel, WeaponType.Swords, damage);

            var expected = WeaponType.Swords;
            var actual = weapon.WeaponType;

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Weapon", "Initialization")]
        public void Constructor_Initialize_Weapon_Should_Have_Correct_Damage()
        {

            var name = "Blade of Steel";
            var reqLevel = 1;
            var damage = 8;
            var weapon = new Weapon(name, reqLevel, WeaponType.Swords, damage);

            var expected = damage;
            var actual = weapon.WeaponDamage;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Armor
        [Fact]
        [Trait("Armor", "Initialization")]
        public void Constructor_Initialize_Armor_Should_Have_Correct_Name()
        {
            var name = "Haed of leather";
            var reqLevel = 1;
            var slot = Slot.Head;
            var type = ArmorType.Leather;
            var attributes = new HeroAttribute(1, 1, 1);
            var armor = new Armor(name, reqLevel, slot, type, attributes);

            var expected = name;
            var actual = armor.Name;

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Armor", "Initialization")]
        public void Constructor_Initialize_Armor_Should_Have_Correct_RequiredLevel()
        {
            var name = "Haed of leather";
            var reqLevel = 1;
            var slot = Slot.Head;
            var type = ArmorType.Leather;
            var attributes = new HeroAttribute(1, 1, 1);
            var armor = new Armor(name, reqLevel, slot, type, attributes);

            var expected = reqLevel;
            var actual = armor.RequiredLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Armor", "Initialization")]
        public void Constructor_Initialize_Armor_Should_Have_Correct_Slot()
        {
            var name = "Haed of leather";
            var reqLevel = 1;
            var slot = Slot.Head;
            var type = ArmorType.Leather;
            var attributes = new HeroAttribute(1, 1, 1);
            var armor = new Armor(name, reqLevel, slot, type, attributes);

            var expected = slot;
            var actual = armor.Slot;

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Armor", "Initialization")]
        public void Constructor_Initialize_Armor_Should_Have_Correct_ArmorType()
        {
            var name = "Haed of leather";
            var reqLevel = 1;
            var slot = Slot.Head;
            var type = ArmorType.Leather;
            var attributes = new HeroAttribute(1, 1, 1);
            var armor = new Armor(name, reqLevel, slot, type, attributes);

            var expected = type;
            var actual = armor.ArmorType;

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Armor", "Initialization")]
        public void Constructor_Initialize_Armor_Should_Have_Correct_Attributes()
        {
            var name = "Haed of leather";
            var reqLevel = 1;
            var slot = Slot.Head;
            var type = ArmorType.Leather;
            var attributes = new HeroAttribute(1, 1, 1);
            var armor = new Armor(name, reqLevel, slot, type, attributes);

            var expected = attributes;
            var actual = armor.ArmorAttribute;

            expected.Should().BeEquivalentTo(actual);
        }
        #endregion

        #region Hero Equipment Armor
        [Fact]
        [Trait("Hero", "Equipment_Armor")]
        public void Hero_Should_Equip_Armor()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var armorName = "Wand";
            var reqLevel = 1;
            var slot = Slot.Head;
            var attribute = new HeroAttribute(1, 1, 1);
            var armor = new Armor(armorName, reqLevel, slot, ArmorType.Cloth, attribute);

            mage.Equip(armor);
            var actual = mage.Equipment[slot] != null;

            Assert.True(actual);
        }
        [Fact]
        [Trait("Hero", "Equipment_Armor_Exception")]
        public void Equip_Armor_Should_Return_With_ItemLevelTooHIghException()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var armorName = "Wand";
            var reqLevel = 10;
            var slot = Slot.Head;
            var attribute = new HeroAttribute(1, 1, 1);
            var armor = new Armor(armorName, reqLevel, slot, ArmorType.Cloth, attribute);


            Assert.Throws<ItemLevelTooHighException>(() => mage.Equip(armor));
        }

        [Fact]
        [Trait("Hero", "Equipment_Armor_Exception")]
        public void Equip_Armor_Should_Return_With_InvalidArmorException()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var armorName = "Wand";
            var reqLevel = 1;
            var slot = Slot.Head;
            var attribute = new HeroAttribute(1, 1, 1);
            var armor = new Armor(armorName, reqLevel, slot, ArmorType.Leather, attribute);

            //Action act = () => mage.Equip(armor);
            //var exception = Assert.Throws<InvalidArmorException>(act);
            //Assert.Equal("expected error message here", exception.Message);

            Assert.Throws<InvalidArmorException>(() => mage.Equip(armor));
        }
        #endregion

        #region Hero Equipment Weapon
        [Fact]
        [Trait("Hero", "Equipment_Weapon")]
        public void Hero_Should_Equip_Weapon()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var WeaponName = "Wand";
            var reqLevel = 1;
            var damage = 1;
            var weapon = new Weapon(WeaponName, reqLevel, WeaponType.Wands, damage);

            mage.Equip(weapon);
            var actual = mage.Equipment[Slot.Weapon] != null;

            Assert.True(actual);
        }

        [Fact]
        [Trait("Hero", "Equipment_Weapon_Exception")]
        public void Equip_Weapon_Should_Return_With_ItemLevelTooHIghException()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var WeaponName = "Wand";
            var reqLevel = 10;
            var damage = 1;
            var weapon = new Weapon(WeaponName, reqLevel, WeaponType.Wands, damage);


            Assert.Throws<ItemLevelTooHighException>(() => mage.Equip(weapon));
        }

        [Fact]
        [Trait("Hero", "Equipment_Weapon_Exception")]
        public void Equip_Weapon_Should_Return_With_InvalidWeaponException()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var WeaponName = "Sword";
            var reqLevel = 2;
            var damage = 1;
            var weapon = new Weapon(WeaponName, reqLevel, WeaponType.Swords, damage);

            Assert.Throws<InvalidWeaponException>(() => mage.Equip(weapon));
        }
        #endregion

        #region TotalAttributes
        [Fact]
        [Trait("Hero", "TotalAttributes")]
        public void TotalAttributes_Should_Be_Calculated_Correct_With_No_Equipment()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var expected = new HeroAttribute(1, 1, 8);
            var actual = mage.TotalAttributes();

            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "TotalAttributes")]
        public void TotalAttributes_Should_Be_Calculated_Correct_With_One_Armor_Piece()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var armorName = "Wand";
            var reqLevel = 1;
            var slot = Slot.Head;
            var attribute = new HeroAttribute(1, 1, 1);
            var armor = new Armor(armorName, reqLevel, slot, ArmorType.Cloth, attribute);

            mage.Equip(armor);

            var expected = new HeroAttribute(2, 2, 9);
            var actual = mage.TotalAttributes();

            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "TotalAttributes")]
        public void TotalAttributes_Should_Be_Calculated_Correct_With_Two_Armor_Pieces()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var armorHeadName = "Head";
            var reqLevel = 1;
            var headSlot = Slot.Head;
            var attributeHead = new HeroAttribute(1, 1, 1);
            var armorHead = new Armor(armorHeadName, reqLevel, headSlot, ArmorType.Cloth, attributeHead);

            var armorBodyName = "Body";
            var bodySlot = Slot.Body;
            var attributeBody = new HeroAttribute(1, 1, 1);
            var armorBody = new Armor(armorBodyName, reqLevel, bodySlot, ArmorType.Cloth, attributeBody);

            mage.Equip(armorHead);
            mage.Equip(armorBody);

            var expected = new HeroAttribute(3, 3, 10);
            var actual = mage.TotalAttributes();

            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "TotalAttributes")]
        public void TotalAttributes_Should_Be_Calculated_Correct_With_Replaced_Armor_Piece()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var reqLevel = 1;
            var headSlot = Slot.Head;

            var armorHeadName = "Head";
            var attributeHead = new HeroAttribute(10, 10, 10);
            var armorHead = new Armor(armorHeadName, reqLevel, headSlot, ArmorType.Cloth, attributeHead);

            var armorReplacementName = "Replacement Body";
            var attributeReplacement = new HeroAttribute(2, 2, 2);
            var armorReplacement = new Armor(armorReplacementName, reqLevel, headSlot, ArmorType.Cloth, attributeReplacement);

            mage.Equip(armorHead);
            mage.Equip(armorReplacement);

            var expected = new HeroAttribute(3, 3, 10);
            var actual = mage.TotalAttributes();

            expected.Should().BeEquivalentTo(actual);
        }
        #endregion

        #region Damage
        [Fact]
        [Trait("Weapon", "Damage")]
        public void Weapon_Damage_Should_Be_Calculated_Correct_With_No_Weapon()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var expected = 1.08;
            var actual = mage.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Weapon", "Damage")]
        public void Weapon_Damage_Should_Be_Calculated_Correct_With_Weapon_Equiped()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var weaponName = "Wand";
            var reqLevel = 1;
            var damage = 2;
            var weaponType = WeaponType.Wands;
            var weapon = new Weapon(weaponName, reqLevel, weaponType, damage);

            mage.Equip(weapon);

            var expected = 2.16;
            var actual = mage.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Weapon", "Damage")]
        public void Weapon_Damage_Should_Be_Calculated_Correct_With_Weapon_And_Armor_Equiped()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var weaponName = "Wand";
            var reqLevel = 1;
            var damage = 2;
            var weaponType = WeaponType.Wands;
            var weapon = new Weapon(weaponName, reqLevel, weaponType, damage);

            var headSlot = Slot.Head;
            var armorHeadName = "Head";
            var attributeHead = new HeroAttribute(1, 1, 1);
            var armorHead = new Armor(armorHeadName, reqLevel, headSlot, ArmorType.Cloth, attributeHead);

            mage.Equip(weapon);
            mage.Equip(armorHead);

            var expected = 2.18;
            var actual = mage.Damage();

            Assert.Equal(expected, actual);
        }
        #endregion

        #region Creation Mage

        [Fact]
        [Trait("Hero", "Mage")]
        public void Constructor_Initialize_Mage_Name_Should_Be_Correct()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var expected = name;
            var actual = mage.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Mage")]
        public void Constructor_Initialize_Mage_Level_Should_Be_Correct()
        {
            var name = "Roar";
            var mage = new Mage(name);

            var expected = 1;
            var actual = mage.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Mage")]
        public void Constructor_Initialize_Mage_Attributes_Should_Be_Correct()
        {
            var attributes = new HeroAttribute(1, 1, 8);

            var name = "Roar";
            var mage = new Mage(name);

            var expected = attributes;
            var actual = mage.LevelAttribute;

            // Using FluentAssertions
            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "Mage")]
        public void Mage_LevelUp_Should_Increase_Level()
        {
            var name = "Roar";
            var mage = new Mage(name);

            mage.LevelUp();

            var expected = 2;
            var actual = mage.Level;

            // Using FluentAssertions
            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Hero", "Mage")]
        public void Mage_LevelUp_Should_Increase_LevelAttributes()
        {
            var name = "Roar";
            var mage = new Mage(name);
            var expected = new HeroAttribute(2, 2, 13);

            mage.LevelUp();

            var actual = mage.LevelAttribute;

            // Using FluentAssertions
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        [Trait("Hero", "Mage")]
        public void Mage_ValidWeaponTypes_Should_Be_Correct()
        {
            var name = "Roar";
            var mage = new Mage(name);
            var expected = new List<WeaponType>() { WeaponType.Staffs, WeaponType.Wands };


            var actual = mage.ValidWeaponTypes;

            // Using FluentAssertions
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        [Trait("Hero", "Mage")]
        public void Mage_ValidArmorTypes_Should_Be_Correct()
        {
            var name = "Roar";
            var mage = new Mage(name);
            var expected = new List<ArmorType>() { ArmorType.Cloth };


            var actual = mage.ValidArmorTypes;

            // Using FluentAssertions
            actual.Should().BeEquivalentTo(expected);
        }
        #endregion

        #region Creation Rogue

        [Fact]
        [Trait("Hero", "Rogue")]
        public void Constructor_Initialize_Rogue_Name_Should_Be_Correct()
        {
            var name = "Roar";
            var rogue = new Rogue(name);

            var expected = name;
            var actual = rogue.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Rogue")]
        public void Constructor_Initialize_Rogue_Level_Should_Be_Correct()
        {
            var name = "Roar";
            var rogue = new Rogue(name);

            var expected = 1;
            var actual = rogue.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Rogue")]
        public void Constructor_Initialize_Rogue_Attributes_Should_Be_Correct()
        {
            var attributes = new HeroAttribute(2, 6, 1);

            var name = "Roar";
            var rogue = new Rogue(name);

            var expected = attributes;
            var actual = rogue.LevelAttribute;

            // Using FluentAssertions
            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "Rogue")]
        public void Rogue_LevelUp_Should_Increase_Level()
        {
            var name = "Roar";
            var rogue = new Rogue(name);

            rogue.LevelUp();

            var expected = 2;
            var actual = rogue.Level;

            // Using FluentAssertions
            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Hero", "Rogue")]
        public void Rogue_LevelUp_Should_Increase_LevelAttributes()
        {
            var name = "Roar";
            var rogue = new Rogue(name);
            var expected = new HeroAttribute(3, 10, 2);

            rogue.LevelUp();

            var actual = rogue.LevelAttribute;

            // Using FluentAssertions
            actual.Should().BeEquivalentTo(expected);
        }
        #endregion

        #region Creation Warrior

        [Fact]
        [Trait("Hero", "Warrior")]
        public void Constructor_Initialize_Warrior_Name_Should_Be_Correct()
        {
            var name = "Roar";
            var warrior = new Warrior(name);

            var expected = name;
            var actual = warrior.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Warrior")]
        public void Constructor_Initialize_Warrior_Level_Should_Be_Correct()
        {
            var name = "Roar";
            var warrior = new Warrior(name);

            var expected = 1;
            var actual = warrior.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Warrior")]
        public void Constructor_Initialize_Warrior_Attributes_Should_Be_Correct()
        {
            var attributes = new HeroAttribute(5, 2, 1);

            var name = "Roar";
            var warrior = new Warrior(name);

            var expected = attributes;
            var actual = warrior.LevelAttribute;

            // Using FluentAssertions
            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "Warrior")]
        public void Warrior_LevelUp_Should_Increase_Level()
        {
            var name = "Roar";
            var warrior = new Warrior(name);

            warrior.LevelUp();

            var expected = 2;
            var actual = warrior.Level;

            // Using FluentAssertions
            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Hero", "Warrior")]
        public void Warrior_LevelUp_Should_Increase_LevelAttributes()
        {
            var name = "Roar";
            var warrior = new Warrior(name);
            var expected = new HeroAttribute(8, 4, 2);

            warrior.LevelUp();

            var actual = warrior.LevelAttribute;

            // Using FluentAssertions
            actual.Should().BeEquivalentTo(expected);
        }
        #endregion

        #region Creation Ranger

        [Fact]
        [Trait("Hero", "Ranger")]
        public void Constructor_Initialize_Ranger_Name_Should_Be_Correct()
        {
            var name = "Roar";
            var ranger = new Ranger(name);

            var expected = name;
            var actual = ranger.Name;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Ranger")]
        public void Constructor_Initialize_Ranger_Level_Should_Be_Correct()
        {
            var name = "Roar";
            var ranger = new Ranger(name);

            var expected = 1;
            var actual = ranger.Level;

            Assert.Equal(expected, actual);
        }
        [Fact]
        [Trait("Hero", "Ranger")]
        public void Constructor_Initialize_Ranger_Attributes_Should_Be_Correct()
        {
            var attributes = new HeroAttribute(1, 7, 1);

            var name = "Roar";
            var ranger = new Ranger(name);

            var expected = attributes;
            var actual = ranger.LevelAttribute;

            // Using FluentAssertions
            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        [Trait("Hero", "Ranger")]
        public void Ranger_LevelUp_Should_Increase_Level()
        {
            var name = "Roar";
            var ranger = new Ranger(name);

            ranger.LevelUp();

            var expected = 2;
            var actual = ranger.Level;

            // Using FluentAssertions
            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Hero", "Ranger")]
        public void Ranger_LevelUp_Should_Increase_LevelAttributes()
        {
            var name = "Roar";
            var ranger = new Ranger(name);
            var expected = new HeroAttribute(2, 12, 2);

            ranger.LevelUp();

            var actual = ranger.LevelAttribute;

            // Using FluentAssertions
            actual.Should().BeEquivalentTo(expected);
        }
        #endregion
    }
}