﻿using RPGHeroes.Equipment;
using RPGHeroes.Equipment.Enum;
using RPGHeroes.Hero;

namespace RPGHeroes
{
    public class Program
    {
        static void Main(string[] args)
        {

            var mage = new Mage("Roar");
            var weapon = new Weapon("Wand of Lightnes", 1, WeaponType.Wands, 14);
            var clothBody = new Armor("Chest of Lightnes", 1, Slot.Body, ArmorType.Cloth, new HeroAttribute(1, 1, 5));
            var clothHead = new Armor("Head of Lightnes", 1, Slot.Head, ArmorType.Cloth, new HeroAttribute(1, 1, 4));
            var clothLegs = new Armor("Legs of Lightnes", 1, Slot.Legs, ArmorType.Cloth, new HeroAttribute(1, 1, 3));

            mage.PrintDisplay();
            mage.LevelUp();
            mage.PrintDisplay();

            var mage2 = new Mage("Knut");
            mage2.PrintDisplay();
            mage2.LevelUp();
            mage2.PrintDisplay();

            var mage3 = new Mage("Rome");
            mage3.PrintDisplay();
            mage3.LevelUp();
            mage3.PrintDisplay();

            mage.Equip(weapon);
            mage.Equip(clothBody);
            mage.Equip(clothHead);
            mage.Equip(clothLegs);

            var warrior = new Warrior("Truls");
            var hammer = new Weapon("Hammer of Greatnes", 1, WeaponType.Hammers, 15);
            var body = new Armor("Chest of Greatnes", 1, Slot.Body, ArmorType.Mail, new HeroAttribute(6, 1, 1));
            var head = new Armor("Head of Greatnes", 1, Slot.Head, ArmorType.Mail, new HeroAttribute(4, 1, 1));
            var legs = new Armor("Legs of Greatnes", 1, Slot.Legs, ArmorType.Mail, new HeroAttribute(3, 1, 1));

            warrior.Equip(hammer);
            warrior.PrintDisplay();
            warrior.Equip(body);
            warrior.Equip(legs);

            warrior.PrintDisplay();
            warrior.LevelUp();
            warrior.Equip(head);
            warrior.PrintDisplay();
            warrior.LevelUp();

            var rogue = new Rogue("Morten");
            var dagger = new Weapon("Dagger of Stealthnes", 1, WeaponType.Daggers, 18);
            var headOfStealth = new Armor("Head of Stealthnes", 1, Slot.Head, ArmorType.Leather, new HeroAttribute(1, 6, 1));
            var bodyOfStealth = new Armor("Chest of Stealthnes", 1, Slot.Body, ArmorType.Leather, new HeroAttribute(1, 5, 1));
            var legsOfStealth = new Armor("Legs of Stealthnes", 1, Slot.Legs, ArmorType.Leather, new HeroAttribute(1, 4, 1));

            rogue.Equip(dagger);
            rogue.Equip(headOfStealth);
            rogue.Equip(bodyOfStealth);
            rogue.Equip(legsOfStealth);
            rogue.LevelUp();

            var ranger = new Ranger("Karl");
            var bowOfRangenes = new Weapon("Bow of Rangenes", 1, WeaponType.Bows, 30);
            var headOfRangenes = new Armor("Head of Rangenes", 1, Slot.Head, ArmorType.Leather, new HeroAttribute(1, 8, 1));
            var bodyOfRangenes = new Armor("Chest of Rangenes", 1, Slot.Body, ArmorType.Leather, new HeroAttribute(1, 5, 1));
            var legsOfRangenes = new Armor("Legs of Rangenes", 1, Slot.Legs, ArmorType.Leather, new HeroAttribute(1, 3, 1));

            ranger.Equip(bowOfRangenes);
            ranger.Equip(headOfRangenes);
            ranger.Equip(bodyOfRangenes);
            ranger.Equip(legsOfRangenes);
            ranger.LevelUp();

            ranger.PrintDisplay();
            rogue.PrintDisplay();
            mage.PrintDisplay();
            warrior.PrintDisplay();

        }
    }
}