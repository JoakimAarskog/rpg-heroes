﻿using RPGHeroes.Equipment.Enum;
using RPGHeroes.Hero;

namespace RPGHeroes.Utils
{
    public static class HeroStats
    {
        public static readonly HeroAttribute MageInitialAttribute = new(1, 1, 8);
        public static readonly HeroAttribute MageIncreaseAttribute = new(1, 1, 5);
        public static readonly List<WeaponType> MageValidWeaponTypes = new() { WeaponType.Staffs, WeaponType.Wands };
        public static readonly List<ArmorType> MageValidArmorTypes = new() { ArmorType.Cloth };

        public static readonly HeroAttribute RogueInitialAttribute = new(2, 6, 1);
        public static readonly HeroAttribute RogueIncreaseAttribute = new(1, 4, 1);
        public static readonly List<WeaponType> RogueValidWeaponTypes = new() { WeaponType.Daggers, WeaponType.Swords };
        public static readonly List<ArmorType> RogueValidArmorTypes = new() { ArmorType.Leather, ArmorType.Mail };


        public static readonly HeroAttribute WarriorInitialAttribute = new(5, 2, 1);
        public static readonly HeroAttribute WarriorIncreaseAttribute = new(3, 2, 1);
        public static readonly List<WeaponType> WarriorValidWeaponTypes = new() { WeaponType.Axes, WeaponType.Hammers, WeaponType.Swords };
        public static readonly List<ArmorType> WarriorValidArmorTypes = new() { ArmorType.Mail, ArmorType.Plate };

        public static readonly HeroAttribute RangerInitialAttribute = new(1, 7, 1);
        public static readonly HeroAttribute RangerIncreaseAttribute = new(1, 5, 1);
        public static readonly List<WeaponType> RangerValidWeaponTypes = new() { WeaponType.Bows };
        public static readonly List<ArmorType> RangerValidArmorTypes = new() { ArmorType.Leather, ArmorType.Mail };

    }
}
