﻿namespace RPGHeroes.Utils.Exeptions
{
    [Serializable]
    public class ItemLevelTooHighException : Exception
    {
        public ItemLevelTooHighException(string? message) : base(message)
        {
        }

        public override string Message => "Item level is too high";
    }
}