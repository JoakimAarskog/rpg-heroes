﻿namespace RPGHeroes.Utils.Exeptions
{
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string? message) : base(message)
        {
        }
    }
}