﻿namespace RPGHeroes.Utils.Exeptions
{
    [Serializable]
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string? message) : base(message)
        {
        }
    }
}