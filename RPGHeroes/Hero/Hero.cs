﻿using RPGHeroes.Equipment;
using RPGHeroes.Equipment.Enum;
using RPGHeroes.Utils.Exeptions;
using System.Text;

namespace RPGHeroes.Hero
{
    public abstract class Hero
    {
        public string Name { get; }
        public int Level { get; set; } = 1;
        public HeroAttribute LevelAttribute { get; }
        public Dictionary<Slot, Item> Equipment { get; }
        public List<WeaponType> ValidWeaponTypes { get; }
        public List<ArmorType> ValidArmorTypes { get; }

        /// <summary>
        /// Creates a Hero
        /// </summary>
        /// <param name="name">Name of the Hero</param>
        /// <param name="levelAttribute">HeroAttribute for the Hero</param>
        /// <param name="validWeaponTypes">List of valid weapon types</param>
        /// <param name="validArmorTypes">List of valid armor types</param>
        public Hero(string name, HeroAttribute levelAttribute, List<WeaponType> validWeaponTypes, List<ArmorType> validArmorTypes)
        {
            Name = name;
            LevelAttribute = new HeroAttribute(levelAttribute);
            ValidWeaponTypes = validWeaponTypes;
            ValidArmorTypes = validArmorTypes;
            Equipment = CreateSlots();
        }

        /// <summary>
        /// Creates Equipment slots.
        /// </summary>
        /// <returns>A Dictionary with equipment slots and Items</returns>
        private static Dictionary<Slot, Item> CreateSlots()
        {
            // Creates a Dictionary
            Dictionary<Slot, Item> result = new();

            // Loops through the Enum Slot and creates a slot for each element.
            foreach (Slot slot in Enum.GetValues(typeof(Slot)))
            {
                result.Add(slot, null!);
            }

            return result;
        }

        /// <summary>
        /// Levels up the Hero. Both Level and HeroAttributes are increased
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// Tries to equip a weapon. Has checks for valid weapon types and required level.
        /// </summary>
        /// <param name="weapon">Weapon that is equiped</param>
        /// <exception cref="InvalidWeaponException">When the Hero can't use that weapon type</exception>
        /// <exception cref="ItemLevelTooHighException">When the Hero's level is too low to equip the Item</exception>
        public void Equip(Weapon weapon)
        {

            // Checks for valid WeaponType
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
                throw new InvalidWeaponException("Your class can not equip this weapon type");

            // Checks if the Hero has high enough level to equip the weapon
            if (Level < weapon.RequiredLevel)
                throw new ItemLevelTooHighException("Your level is too low to equip this item");

            Equipment[Slot.Weapon] = weapon;
        }

        /// <summary>
        /// Tries to equip an Armor piece. Has checks for valid armor types and required level.
        /// </summary>
        /// <param name="armor">Armor that is equiped</param>
        /// <exception cref="InvalidArmorException">When the Hero can't use that armor type</exception>
        /// <exception cref="ItemLevelTooHighException">When the Hero's level is too low to equip the Item</exception>
        public void Equip(Armor armor)
        {
            // Checks for valid ArmorType
            if (!ValidArmorTypes.Contains(armor.ArmorType))
                throw new InvalidArmorException("Your class can not equip this armor type");

            // Checks if the Hero has high enough level to equip the arom
            if (Level < armor.RequiredLevel)
                throw new ItemLevelTooHighException("Your level is too low to equip this item");

            Equipment[armor.Slot] = armor;
        }


        /// <summary>
        /// Damage method that is overriden in each Hero class
        /// </summary>
        /// <returns>The calculated damage</returns>
        public abstract double Damage();

        /// <summary>
        /// Takes the weapon damage and the appropriate HeroAttribute based on class and calculates the damage.
        /// </summary>
        /// <returns>The damage based on weapon damage and HeroAttributes.</returns>
        public double Damage(double damagingAttribute)
        {
            var damageFactor = (1 + (double)damagingAttribute / 100);
            var weapon = (Weapon)Equipment[Slot.Weapon];

            // If the Hero has a weapon equiped, it returns the weapon damage multiplied by the damage factor. If not, damagefactor is multipled by 1
            return weapon != null ? (double)(weapon.WeaponDamage * damageFactor) : (double)(1 * damageFactor); // Magic numbers are bad!!
        }


        /// <summary>
        /// Calculates the TotalAttributes of the Hero based on level and armor.
        /// </summary>
        /// <returns>Returns a new HeroAttribute with the LevelAttribute plus the attributes from the armor.</returns>
        public HeroAttribute TotalAttributes()
        {

            // Set's the attributes to be the same as the level.
            var equipmentStrength = LevelAttribute.Strength;
            var equipmentDexterity = LevelAttribute.Dexterity;
            var equipmentIntellegence = LevelAttribute.Intelligence;

            // Loops through the equipment
            foreach (var entry in Equipment)
            {
                // If it's a weapon, skip it
                if (entry.Value != null && entry.Value.Slot != Slot.Weapon)
                {
                    // Cast the Item to Armor
                    var armor = (Armor)entry.Value;
                    equipmentStrength += armor.ArmorAttribute.Strength;
                    equipmentDexterity += armor.ArmorAttribute.Dexterity;
                    equipmentIntellegence += armor.ArmorAttribute.Intelligence;
                }
            }

            return new HeroAttribute(equipmentStrength, equipmentDexterity, equipmentIntellegence);
        }

        /// <summary>
        /// Creates a string with the correct data
        /// </summary>
        /// <returns>A string with the correct data</returns>
        public string Display()
        {
            StringBuilder stringBuilder = new();

            stringBuilder.AppendLine($"Name: {Name}");
            stringBuilder.AppendLine($"Class: {GetType().Name}");
            stringBuilder.AppendLine($"Level: {Level}");
            stringBuilder.AppendLine($"Equipment: ");
            foreach (var entry in Equipment)
            {
                stringBuilder.Append($"  {entry.Key}: ");
                stringBuilder.AppendLine(entry.Value?.Name ?? "");
            }
            stringBuilder.AppendLine($"Total strength: {TotalAttributes().Strength}");
            stringBuilder.AppendLine($"Total dexterity: {TotalAttributes().Dexterity}");
            stringBuilder.AppendLine($"Total intelligence: {TotalAttributes().Intelligence}");
            stringBuilder.AppendLine($"Damage: {Damage()}");

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Prints out the display
        /// </summary>
        public void PrintDisplay()
        {
            Console.WriteLine(Display());
        }
    }
}
