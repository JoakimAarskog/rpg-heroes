﻿using RPGHeroes.Utils;

namespace RPGHeroes.Hero
{
    public class Mage : Hero
    {
        /// <summary>
        /// Creates a Hero
        /// </summary>
        /// <param name="name">Name of the Hero</param>
        public Mage(string name) : base(name, HeroStats.MageInitialAttribute, HeroStats.MageValidWeaponTypes, HeroStats.MageValidArmorTypes)
        {
        }

        /// <summary>
        /// Gets the TotalAttributes with the correct attribute.
        /// </summary>
        /// <returns>The total damage</returns>
        public override double Damage()
        {
            var damagingAttribute = TotalAttributes().Intelligence;
            return Damage(damagingAttribute);
        }

        /// <summary>
        /// Levels up the Hero. Both Level and HeroAttributes are increased
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            LevelAttribute.CalculateHeroAttribute(HeroStats.MageIncreaseAttribute);
        }

    }
}
