﻿namespace RPGHeroes.Hero
{
    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// Creates a HeroAttribute
        /// </summary>
        /// <param name="strength">Strength value</param>
        /// <param name="dexterity">Dexterity value</param>
        /// <param name="intelligence">Intelligence value</param>
        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Creates a HeroAttribute from an existing HeroAttribute
        /// </summary>
        /// <param name="heroAttribute"></param>
        public HeroAttribute(HeroAttribute heroAttribute)
        {
            Strength = heroAttribute.Strength;
            Dexterity = heroAttribute.Dexterity;
            Intelligence = heroAttribute.Intelligence;
        }

        /// <summary>
        /// Adds attributes
        /// </summary>
        /// <param name="heroAttribute">HeroAttributes to be added</param>
        public void CalculateHeroAttribute(HeroAttribute heroAttribute)
        {
            Strength += heroAttribute.Strength;
            Dexterity += heroAttribute.Dexterity;
            Intelligence += heroAttribute.Intelligence;
        }
    }
}