﻿using RPGHeroes.Utils;

namespace RPGHeroes.Hero
{
    public class Warrior : Hero
    {
        /// <summary>
        /// Creates a Hero
        /// </summary>
        /// <param name="name">Name of the Hero</param>
        public Warrior(string name) : base(name, HeroStats.WarriorInitialAttribute, HeroStats.WarriorValidWeaponTypes, HeroStats.WarriorValidArmorTypes)
        {
        }

        /// <summary>
        /// Gets the TotalAttributes with the correct attribute.
        /// </summary>
        /// <returns>The total damage</returns>
        public override double Damage()
        {
            var damagingAttribute = TotalAttributes().Strength;
            return Damage(damagingAttribute);
        }

        /// <summary>
        /// Levels up the Hero. Both Level and HeroAttributes are increased
        /// </summary>
        public override void LevelUp()
        {
            Level++;
            LevelAttribute.CalculateHeroAttribute(HeroStats.WarriorIncreaseAttribute);
        }
    }
}
