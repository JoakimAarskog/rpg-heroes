﻿using RPGHeroes.Equipment.Enum;
using RPGHeroes.Hero;

namespace RPGHeroes.Equipment
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public HeroAttribute ArmorAttribute { get; set; }

        public Armor(string name, int requiredLevel, Slot slot, ArmorType armorType, HeroAttribute armorAttribute) : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorAttribute = armorAttribute;
        }
    }
}

