﻿namespace RPGHeroes.Equipment.Enum
{
    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}