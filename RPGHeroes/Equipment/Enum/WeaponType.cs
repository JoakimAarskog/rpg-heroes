﻿namespace RPGHeroes.Equipment.Enum
{
    public enum WeaponType
    {
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands,
    }
}