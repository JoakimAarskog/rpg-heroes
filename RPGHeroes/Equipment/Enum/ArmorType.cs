﻿namespace RPGHeroes.Equipment.Enum
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate,
    }

}