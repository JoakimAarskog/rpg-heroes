﻿using RPGHeroes.Equipment.Enum;

namespace RPGHeroes.Equipment
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public double WeaponDamage { get; set; }

        public Weapon(string name, int requiredLevel, WeaponType weaponType, int weaponDamage) : base(name, requiredLevel, Slot.Weapon)
        {
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }
    }
}
